#=
 * File: Planar.jl
 * Project: src
 * File Created: Tuesday, 22nd October 2019 10:16:30 am
 * Author: Josiah Putman (joshikatsu@gmail.com); Luyang Zhao (luyang.zhao.gr@dartmouth.edu)
 * -----
 * Last Modified: 3rd Nov 2020
 * Modified By: Luyang Zhao (luyang.zhao.gr@dartmouth.edu)
=#

module Planar

#region imports
import PyCall, PyPlot
import Distances

using MetricTools, MetricTools.Visualization, MetricTools.Geometry,
    MetricTools.GJKTools, MetricTools.EarClipping
using ..MetricSpaces
# using SignedDistanceFields, JLD

#endregion imports

export PlanarSpace, Space, load_polygons, space, plot, cert, dmetric


"""
2D space with polygonal obstacles
"""
mutable struct PlanarSpace <: Space
    dim::Integer
    bounds::Array{Bound}
    obstacles::Array{Polygon}
    rawobstacles::Array{Polygon}
    sdf::Array{Float64,2}
    df_size::Integer
    df_interval::Float64 #used for sdf
    
    function PlanarSpace(;
        bounds=[Bound(0, 1) for _ in 1:2]::Array{Bound},
        filename=""::String,
        triangulate::Bool=false,
        sdf::Array{Float64,2}=Array{Float64}(undef, 0, 2)
    )
        rawobstacles::Array{Polygon} = !isempty(filename) ? load_polygons(filename) : []
        obstacles::Array{Polygon} = rawobstacles
        if triangulate
            obstacles = []
            for obstacle in rawobstacles
                push!(obstacles, earclip(deepcopy(obstacle); if_reverse=false)...)
                push!(obstacles, earclip(deepcopy(obstacle); if_reverse=true)...)  #!! added this line
            end
        end

        df_size::Integer = size(sdf)[1]
        df_interval::Float64 =  isempty(sdf) ? 0.1 : (bounds[1].hi - bounds[1].lo)/(df_size-1) 
        
        return new(
            2,
            bounds,
            obstacles,
            rawobstacles,
            sdf,
            df_size,
            df_interval
        )
    end
end


"""
Plots the space
"""
function MetricTools.plot(space::PlanarSpace; ax=PyCall.PyNULL()::PyCall.PyObject)
    plot(space.obstacles; ax=ax)
end

"""
Calculates L2 distance between two points in the planar space
"""
MetricTools.metric(space::PlanarSpace, point1::Point, point2::Point) =
    Distances.euclidean(point1, point2)
    
"""
Determines if a point is free
"""
function MetricSpaces.isfree(space::PlanarSpace, point::Point)::Bool
    if !(point in space.bounds) return false end

    for polygon in space.obstacles
        if contains(polygon, point)
            return false
        end
    end
    
    return true
end

"""
Determines if two points can be connected
"""
function MetricSpaces.localplanner(space::PlanarSpace, point1::Point, point2::Point)::Bool
    if !(isfree(space, point1) && isfree(space, point2)) return false end
    line = (point1, point2)
    for polygon in space.obstacles
        if intersects(polygon, line; onedge=false)
            return false
        end
    end
    return true
end


"""
convert point into index for distance field
"""
function point_dis_from_sdf(space::PlanarSpace, point::Point)
    df_size::Integer = space.df_size
    interval::Float64 = space.df_interval
    x_i::Integer = div(point[1], interval) + 1
    x_extra::Float64 = mod(point[1], interval)
    
    y_i::Integer = div(point[2], interval) + 1
    y_extra::Float64 = mod(point[2], interval)

    if x_i + 1 > df_size
        if y_i + 1 > df_size
            return space.sdf[x_i, y_i]
        else
            return (space.sdf[x_i, y_i]*(interval-y_extra)+space.sdf[x_i, y_i+1]*y_extra)/interval
        end
    elseif y_i + 1 > df_size
        return (space.sdf[x_i, y_i]*(interval-x_extra)+space.sdf[x_i+1, y_i]*x_extra)/interval
    else
        temp = min(space.sdf[x_i, y_i], space.sdf[x_i+1, y_i], space.sdf[x_i, y_i+1], space.sdf[x_i+1, y_i+1])
        # Once one of it is neighbor is blocked, consider current point is blocked
        if temp < 0 return temp end 
        leftup_area::Float64 = x_extra*(interval-y_extra)
        leftdown_area::Float64 = x_extra*y_extra
        rightup_area::Float64 = (interval-x_extra)*(interval-y_extra)
        rightdown_area::Float64 = (interval-x_extra)*y_extra
        return (leftup_area*space.sdf[x_i+1, y_i] + leftdown_area*space.sdf[x_i+1, y_i+1] + rightup_area*space.sdf[x_i, y_i] + rightdown_area*space.sdf[x_i, y_i+1])/(interval^2)
    end
end


"""
Perform GJK to get a lower bound on the distance to nearest obstacle
"""
function MetricSpaces.cert(space::PlanarSpace, point::Point)::Float64
    if isempty(space.sdf) 
        return gjkdist(space.obstacles, point) 
    else
        return point_dis_from_sdf(space, point) 
    end
end

"""
Perform GJK to get the penetration depth
"""
function MetricSpaces.obscert(space::PlanarSpace, point::Point; delta::Float64=0.0)::Float64
    if isempty(space.sdf) 
        return 1.1*penetratedepth(space.obstacles, point)+delta/2.0
    else
        return -1.1*point_dis_from_sdf(space, point)+delta/2.0 ##!todo, do I need to add delta/2???
    end
end

function MetricSpaces.dmetric(space::PlanarSpace)
    return Distances.Euclidean()
end
end
