#=
 * File: Arms.jl
 * Project: src
 * File Created: Tuesday, 22nd October 2019 10:16:30 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 22nd April 2020 8:59:00 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module Arms

#region imports
import PyCall
import PyPlot
import Distances
import JSON2

using MetricTools, MetricTools.Visualization, MetricTools.Geometry,
MetricTools.GJKTools, MetricTools.EarClipping
using ..MetricSpaces
#endregion imports

export ArmSpace, Arm, Space, set!, cert, load_arm

const ARM_IMG_DIR = "data/output/images/arms/"
const ARM_VID_DIR = "data/output/videos/arms/"

"""
Called when the module is loaded, makes sure all directories are created
"""
function __init__()
    mkpath(ARM_IMG_DIR)
    mkpath(ARM_VID_DIR)
end

mutable struct Arm
    lengths::Array{Float64} 
    angles::Point
    lines::Array{Line}
    base::Point

    function Arm(lengths::Array{Float64}, angles::Point, base::Point)::Arm
        arm::Arm = new(
            lengths,
            angles,
            [(base, base) for _ in lengths],    # inits the line placeholders
            base
        )
        update!(arm)
        return arm
    end
end
    
"""
Sets the linked arm to a configuration
"""
function set!(arm::Arm, point::Point)
    if arm.angles != point 
        arm.angles = point
        update!(arm)
    end
end




"""
Updates the linked arm, forward kinematics
"""
function update!(arm::Arm) 
    sumangle::Float64 = 0
    for i in eachindex(arm.angles)
        sumangle += arm.angles[i]
        delta::Array{Float64} = [
            arm.lengths[i] * cos(sumangle),
            arm.lengths[i] * sin(sumangle)
        ]
        
        # calculate the endpoint
        arm.lines[i] = (arm.lines[i][1], arm.lines[i][1] .+ delta)

        # copy the endpoint to be the start point of the next
        if i < lastindex(arm.angles)
            arm.lines[i + 1] = (arm.lines[i][2], arm.lines[i][2])
        end
    end
end

"""
Loads an arm from a config file
"""
function load_arm(filename::String)::Arm
    open(filename) do file
        jsonobj = JSON2.read(file)
        return Arm(
            convert(Array{Float64}, jsonobj.lengths),
            convert(Array{Float64}, jsonobj.angles),
            convert(Point, jsonobj.base))
    end
end

"""
Formats the plot
"""
# function format_plot()
#     PyPlot.cla()
#     PyPlot.autoscale(false)
#     ax = PyPlot.gca()
#     ax.set_ylim(ymin=-0.1, ymax=1.5)
#     ax.set_xlim(xmin=-0.4, xmax=1.2)
#     ax.set_aspect("equal")
# end

function format_plot(ax::PyCall.PyObject)
    PyPlot.cla()
    PyPlot.autoscale(false)
    # ax = PyPlot.gca()
    # ax.set_ylim(ymin=-1, ymax=2.2)
    # ax.set_xlim(xmin=-1, xmax=2.2)

    ax.set_ylim(ymin=-1, ymax=5)
    ax.set_xlim(xmin=-1, xmax=5)

    ax.set_aspect("equal")
end


"""
Plots a linked arm
"""
function MetricTools.plot(
        arm::Arm;
        ax=PyCall.PyNULL()::PyCall.PyObject,
        color::Tuple,
        alpha::Float64=1.,
        save=false::Bool,
        i=0::Integer)
    plot(arm.lines; ax=ax, color=color, width=2, alpha=alpha)
    if save PyPlot.savefig("$(ARM_IMG_DIR)img$(lpad(i, 4, '0')).png") end
end

"""
Arm Link space with polygonal obstacles
"""
mutable struct ArmSpace <: Space
    dim::Integer
    bounds::Array{Bound}
    arm::Arm
    obstacles::Array{Polygon}
    rawobstacles::Array{Polygon}
    sdf::Array{Float64,2}
    df_size::Integer
    df_interval::Float64 #used for sdf
    ws_bounds::Array{Bound}
    
    function ArmSpace(filename::String, obstfilename::String;
            bounds=Bound[]::Array{Bound}, # bounds for c-space
            triangulate::Bool=false,
            sdf::Array{Float64,2}=Array{Float64}(undef, 0, 2))  
        arm::Arm = load_arm(filename)
        rawobstacles::Array{Polygon} = Geometry.load_polygons(obstfilename)
        obstacles::Array{Polygon} = rawobstacles
        
        if triangulate
            obstacles = []
            for obstacle in rawobstacles
                push!(obstacles, earclip(deepcopy(obstacle); if_reverse=false)...)
                push!(obstacles, earclip(deepcopy(obstacle); if_reverse=true)...)  #!! added this line
            end
        end

        dim::Integer = length(arm.lengths)
        if isempty(bounds) bounds = [Bound(-pi, pi) for _ in 1:dim] end
        df_size::Integer = size(sdf)[1]
        ws_bounds::Array{Bound} = isempty(sdf) ? Bound[] : calculate_ws_bounds(arm)
        df_interval::Float64 =  isempty(sdf) ? 0.1 : (ws_bounds[1].hi - ws_bounds[1].lo)/(df_size-1) 
        return new(dim, bounds, arm, obstacles, rawobstacles, sdf, df_size, df_interval, ws_bounds)
    end
end

"""
Calculate the bounds of workspace
"""
function calculate_ws_bounds(arm::Arm)::Array{Bound}
    l::Float64 = sum(arm.lengths)
    return [Bound(p-l, p+l) for p in arm.base] 
end
"""
Metric distance between two points on an arm
"""
function MetricTools.metric(space::ArmSpace, point1::Point, point2::Point)
    return Distances.Chebyshev()(point1, point2)
end

"""
Gets the metric function 
"""
function MetricSpaces.dmetric(space::ArmSpace)
    return Distances.Chebyshev()
end

"""
Determines if a point is free
"""
function MetricSpaces.isfree(space::ArmSpace, point::Point)::Bool
    set!(space.arm, point)
    for line in space.arm.lines
        for polygon in space.obstacles
            if Geometry.intersects(polygon, line)
                return false
            end
        end
    end
    return true
end

"""
Determines if two points can be connected
"""
function MetricSpaces.localplanner(space::ArmSpace, point1::Point, point2::Point)::Bool
    n::Integer = round(metric(space, point1, point2) * 20) + 1
    points = Math.interpolate(point1, point2, n)
    for point in points
        if !isfree(space, point)
            return false
        end
    end
    return true
end


"""
Calculate vmax for current point, which is (θ1, θ2 ...)
"""
function calculate_vmax(space::ArmSpace, point::Point)::Float64
    velocity = [1.0 for i in 1:space.dim] ##TODO: define velocity other places
    if space.dim == 2
        l1 = space.arm.lengths[1]
        l2 = space.arm.lengths[2]
        x_theta2 = -l2*sin(point[1]+point[2])
        x_theta1 = -l1*sin(point[1])+x_theta2

        y_theta2 = l2*cos(point[1]+point[2])
        y_theta1 = l1*cos(point[1])+y_theta2
        
        v_x = x_theta1*velocity[1]+x_theta2*velocity[2]
        v_y = y_theta1*velocity[1]+y_theta2*velocity[2]
        v_max = sqrt(v_x^2+v_y^2)
        return v_max
    elseif space.dim == 3
        l1 = space.arm.lengths[1]
        l2 = space.arm.lengths[2]
        l3 = space.arm.lengths[3]
        x_theta3 = -l3*sin(point[1]+point[2]+point[3])
        x_theta2 = -l2*sin(point[1]+point[2])+x_theta3
        x_theta1 = -l1*sin(point[1])+x_theta2

        y_theta3 = l3*cos(point[1]+point[2]+point[3])
        y_theta2 = l2*cos(point[1]+point[2])+y_theta3
        y_theta1 = l1*cos(point[1])+y_theta2

        v_x = x_theta1*velocity[1]+x_theta2*velocity[2]+x_theta3*velocity[3]
        v_y = y_theta1*velocity[1]+y_theta2*velocity[2]+y_theta3*velocity[3]
        v_max = sqrt(v_x^2+v_y^2)
        return v_max
    elseif space.dim == 4
        l1 = space.arm.lengths[1]
        l2 = space.arm.lengths[2]
        l3 = space.arm.lengths[3]
        l4 = space.arm.lengths[4]
        x_theta4 = -l4*sin(point[1]+point[2]+point[3]+point[4])
        x_theta3 = -l3*sin(point[1]+point[2]+point[3])+x_theta4
        x_theta2 = -l2*sin(point[1]+point[2])+x_theta3
        x_theta1 = -l1*sin(point[1])+x_theta2

        y_theta4 = l4*cos(point[1]+point[2]+point[3]+point[4])
        y_theta3 = l3*cos(point[1]+point[2]+point[3])+y_theta4
        y_theta2 = l2*cos(point[1]+point[2])+y_theta3
        y_theta1 = l1*cos(point[1])+y_theta2

        v_x = x_theta1*velocity[1]+x_theta2*velocity[2]+x_theta3*velocity[3]+x_theta4*velocity[4]
        v_y = y_theta1*velocity[1]+y_theta2*velocity[2]+y_theta3*velocity[3]+y_theta4*velocity[4]
        v_max = sqrt(v_x^2+v_y^2)
        return v_max
    end
        
end

"""
Computes min distance between polygons and lines using distance field
"""
function min_cert_dis_from_sdf(space::ArmSpace) 
    mindist::Float64 = typemax(Float64)
    lines = space.arm.lines
    for line in lines
        for (i,j) in zip(range(line[1][1],line[2][1],length=5), range(line[1][2],line[2][2],length=5))
            point::Point = [i,j]
            dist::Float64 = point_dis_from_sdf(space, point)
            if dist < 0 return -0.1 end
            if dist < mindist mindist = dist end
        end
    end
    return mindist
end



"""
Computes max penetratedepth between polygons and lines using distance field
"""
function max_pene_depth_from_sdf(space::ArmSpace)  #Return positive number
    maxdist::Float64 = typemin(Float64)
    lines = space.arm.lines
    for line in lines
        for (i,j) in zip(range(line[1][1],line[2][1],length=5), range(line[1][2],line[2][2],length=5))
            point::Point = [i,j]
            dist::Float64 = point_dis_from_sdf(space, point)
            if dist >= 0 continue end
            if dist < 0 && (-1*dist) > maxdist  maxdist = (-1*dist) end
        end
    end
    return maxdist
end

"""
convert point into index for distance field
"""
function point_dis_from_sdf(space::ArmSpace, point::Point) # This point is in workspace
    df_size::Integer = space.df_size
    interval::Float64 = space.df_interval
    ws_bounds::Array{Bound} = space.ws_bounds
    converted_p_1 = (point[1] - ws_bounds[1].lo)  ##Only different part
    x_i::Integer = div(converted_p_1, interval) + 1
    x_extra::Float64 = mod(point[1], interval)
    
    converted_p_2 = (point[2] - ws_bounds[2].lo) ## Only different part
    y_i::Integer = div(converted_p_2, interval) + 1
    y_extra::Float64 = mod(point[2], interval)

    if x_i + 1 > df_size
        if y_i + 1 > df_size
            return space.sdf[x_i, y_i]
        else
            return (space.sdf[x_i, y_i]*(interval-y_extra)+space.sdf[x_i, y_i+1]*y_extra)/interval
        end
    elseif y_i + 1 > df_size
        return (space.sdf[x_i, y_i]*(interval-x_extra)+space.sdf[x_i+1, y_i]*x_extra)/interval
    else
        temp = min(space.sdf[x_i, y_i], space.sdf[x_i+1, y_i], space.sdf[x_i, y_i+1], space.sdf[x_i+1, y_i+1])
        # Once one of it is neighbor is blocked, consider current point is blocked
        if temp < 0 return temp end 
        leftup_area::Float64 = x_extra*(interval-y_extra)
        leftdown_area::Float64 = x_extra*y_extra
        rightup_area::Float64 = (interval-x_extra)*(interval-y_extra)
        rightdown_area::Float64 = (interval-x_extra)*y_extra
        return (leftup_area*space.sdf[x_i+1, y_i] + leftdown_area*space.sdf[x_i+1, y_i+1] + rightup_area*space.sdf[x_i, y_i] + rightdown_area*space.sdf[x_i, y_i+1])/(interval^2)
    end

end

"""
Perform GJK to get a lower bound on the distance to nearest obstacle
"""
function MetricSpaces.cert(space::ArmSpace, point::Point)::Float64
    set!(space.arm, point) 
    v_max::Float64 = 0
    ## assume all angular velocity is 1   w*l
    mindis = isempty(space.sdf) ? gjkdist(space.obstacles, space.arm.lines) : min_cert_dis_from_sdf(space) 
    v_max = calculate_vmax(space, point)

    if mindis < 0  return mindis end
    return mindis/v_max


end

"""
Perform GJK to get the penetration depth
"""
function MetricSpaces.obscert(space::ArmSpace, point::Point; delta::Float64=0.0)::Float64
    set!(space.arm, point)
    v_max::Float64 = 0
    ## assume all angular velocity is 1   w*l
    mindis = isempty(space.sdf) ? penetratedepth(space.obstacles, space.arm.lines) : max_pene_depth_from_sdf(space) 
    v_max = calculate_vmax(space, point)
    return (mindis+delta/2.0)/v_max 
    # return (mindis+delta/2.0)/v_max 

end

"""
Plots a linked arm given a path
"""
function MetricTools.plot(
        space::ArmSpace,
        path::Array{Point};
        ax=PyCall.PyNULL()::PyCall.PyObject,
        tstep::Float64=0.025,
        video::Bool=false,
        filename="arms")
    # println("Plotting linked arm path $path")
    
    if ax == PyCall.PyNULL() ax = PyPlot.gca() end
    # interpolate the path based on the timestep
    numtsteps::Array{Integer} = []
    totalsteps::Integer = sum(numtsteps)
    interpath::Array{Point} = []
    for i in firstindex(path):(lastindex(path) - 1)
        push!(numtsteps, metric(space, path[i], path[i + 1])÷ tstep + 1)
        push!(interpath, Math.interpolate(path[i], path[i + 1], numtsteps[i])...)
    end
    
    len::Integer = length(interpath)

    # plots with colors
    format_plot(ax)
    plot(space.obstacles; ax=ax)
    if isempty(path) return end
    for (i, p) in enumerate(interpath)
        set!(space.arm, p)
        plot(space.arm; ax=ax, color=(i/len, 0, 1 - i/len), alpha=0.15)
    end

    set!(space.arm, first(path))
    plot(space.arm; ax=ax, color=(0, 0, 1), alpha=0.7)
    set!(space.arm, last(path))
    plot(space.arm; ax=ax, color=(1, 0, 0), alpha=0.7)

    # if video
    if !video return end
    
    # Add to remove all the images in the folder firstly
    for f in filter(x -> endswith(x, "png"), readdir(ARM_IMG_DIR))
        rm("$(ARM_IMG_DIR)$(f)")
    end
    
    for (i, p) in enumerate(interpath)
        format_plot(ax)
        set!(space.arm, p)
        if !isfree(space, p) println("Path has points that aren't free. Increase size of PRM.") end
        plot(space.obstacles; ax=ax)
        plot(space.arm; ax=ax, save=true, i=i)
    end
    
    compilemp4(ARM_IMG_DIR;
        outdir=ARM_VID_DIR,
        outfile="$filename.mp4",
        framerate=20)
end

"""
Plots an arm link space
"""
function MetricTools.plot(space::ArmSpace; ax=PyCall.PyNULL()::PyCall.PyObject)
    if ax == PyCall.PyNULL() ax = PyPlot.gca() end
    plot(space.arm; ax=ax, color=(0, 0, 0))
    plot(space.obstacles; ax=ax)
end
end
