#=
 * File: Samplers.jl
 * Project: src
 * File Created: Tuesday, 22nd October 2019 10:16:30 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 19th April 2020 2:50:20 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module Samplers

#region imports
using MetricTools
using ..MetricSpaces
#region imports

export uniform_random, grid, gridl, sample_inbounds

"""
Returns a sample in bounds, not necessarily free
"""
@inline function sample_inbounds(space::Space)::Point
    return [rand() * (b.hi - b.lo) + b.lo for b in space.bounds]
end

"""
Uniform random sampling in a given space
"""
function uniform_random(space::Space, n::Integer;
        bounds=Bound[]::Array{Bound},
        maxattempts::Integer=n*3 + 10,
        verbose::Bool=false)::Array{Point}
    
    points::Array{Point} = []
    point::Point = []
    attempts::Integer = 0
    if isempty(bounds) bounds = space.bounds end
    
    while length(points) < n
        point = [rand() * (b.hi - b.lo) + b.lo for b in bounds]
        if isfree(space, point) push!(points, point) end
        
        attempts += 1
        if attempts > maxattempts
            if verbose
                println("Sampler failed to find $n points in $bounds ($(length(points)) found).")
            end
            return points
        end
    end

    return points
end

"""
Generates a grid of samples over the space
Each dimension has l samples along its axis
"""
function gridl(bounds::Array{Bound}, length::Integer)::Array{Point}
    points::Array{Point} = []
    for tup in Iterators.product(
            [b.lo:((b.hi - b.lo) / length):b.hi
            for b in bounds]...)
        point::Point = [v for v in tup]
        push!(points, point)
    end
    
    return points
end

"""
Generates a grid of samples over the space
Each dimension has l samples along its axis
"""
function gridl(space::Space, length::Integer;
        bounds::Array{Bound} = Bound[])::Array{Point}
        
    if isempty(bounds) bounds = space.bounds end
    
    points::Array{Point} = []
    for tup in Iterators.product(
            [b.lo:((b.hi - b.lo) / length):b.hi
            for b in bounds]...)
        point::Point = [v for v in tup]
        if isfree(space, point) push!(points, point) end
    end
    
    return points
end

"""
Computes a grid producing at most n samples
"""
grid(space::Space, n::Integer;
        bounds::Array{Bound} = Bound[])::Array{Point} = 
    gridl(space, round(Int, n ^ (1 / space.dim)); bounds=bounds)

end