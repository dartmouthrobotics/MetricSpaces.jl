#=
 * File: interactive.jl
 * Project: test
 * File Created: Tuesday, 3rd December 2019 4:02:10 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 17th March 2020 5:08:22 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module WorldBuilders

using MetricTools
import PyCall, PyPlot, JSON2
export WorldBuilder

mutable struct WorldBuilder
    polygons::Array{Polygon}
    fig::PyPlot.Figure
    ax::PyCall.PyObject

    function WorldBuilder()
        wb::WorldBuilder = new(Polygon[Polygon([])], PyPlot.subplots()...)

        onclick::Function = getonclick(wb)
        onpress::Function = getonpress(wb)
        wb.fig.canvas.mpl_connect("button_press_event", onclick)
        wb.fig.canvas.mpl_connect("key_press_event", onpress)
        draw(wb)
        
        println("Welcome to WorldBuilder!")
        println("Use left click to add points to current polygon and right click to start a new polygon.")
        println("Hit enter to write the polygons to './newworld.json'.")
        return wb
    end
end

function round05(x::Float64)
    x *= 2
    x = round(x; digits=1)
    return x / 2
end

function draw(wb::WorldBuilder)
    wb.ax.clear()

    PyPlot.xlim(0, 1)
    PyPlot.ylim(0, 1)
    
    wb.ax.set_xticks(collect(0:0.2:1))
    wb.ax.set_xticks(collect(0:0.05:1); minor=true)
    wb.ax.set_yticks(collect(0:0.2:1))
    wb.ax.set_yticks(collect(0:0.05:1); minor=true)
    wb.ax.grid(true; which="both")
    wb.ax.set_aspect("equal")

    println(first(wb.polygons))
    if !isempty(first(wb.polygons)) plot(wb.polygons; ax=wb.ax, points=true) end
    wb.fig.canvas.draw()
end

function getonclick(wb::WorldBuilder)
    function onleftclick(event)
        point::Point = Point(round05.([event.xdata, event.ydata]))
        push!(last(wb.polygons), point)
        println(wb.polygons)
        draw(wb)
    end

    function onrightclick(event)
        if !isempty(last(wb.polygons)) push!(wb.polygons, Polygon([])) end
    end

    function onclick(event)
        if event.button == 1 onleftclick(event)
        elseif event.button == 3 onrightclick(event)
        else println("Invalid command.") end
    end
    return onclick
end

function getonpress(wb::WorldBuilder)
    function onenter(event)
        jsonstr::String = JSON2.write(wb.polygons)
        open("newworld.json", "w") do f
            write(f, jsonstr)
        end
        println("Saved to './newworld.json'.")
    end

    function onbackspace(event)
        if !isempty(last(wb.polygons)) pop!(last(wb.polygons)) end
        draw(wb)
    end

    function onpress(event)
        println(event.key)
        if event.key == "enter" onenter(event)
        elseif event.key == "backspace" onbackspace(event)
        else println("Invalid command.") end
    end
    return onpress
end
end
