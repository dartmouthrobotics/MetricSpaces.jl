#=
 * File: MetricSpaces.jl
 * Project: src
 * File Created: Tuesday, 22nd October 2019 10:16:30 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 21st March 2020 1:59:47 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module MetricSpaces

using MetricTools, MetricTools.MetricGraphs
export
    Planar,
    Arms,
    ReedsShepp,
    Space,
    Samplers,
    isfree,
    localplanner,
    getlines,
    find,
    cert,
    obscert,
    dmetric

abstract type Space end
@mustimplement isfree(space::Space, point::Point)
@mustimplement localplanner(space::Space, point1::Point, point2::Point)
@mustimplement cert(space::Space, point::Point)
@mustimplement obscert(space::Space, point::Point)
@mustimplement MetricTools.metric(space::Space, point1::Point, point2::Point)
@mustimplement dmetric(space::Space)
@inline MetricTools.metric(space::Space, path::Array{Point}) =
    metric((p1, p2) -> metric(space, p1, p2), path)

function MetricTools.contains(space::Space, point::Point)
    for d in eachindex(point)
        bound::Bound = space.bounds[d]
        if !(bound.lo <= point[d] <= bound.hi)
            return false
        end
    end
    return true
end
    
abstract type Partition end
@mustimplement getlines(partition::Partition, bounds::Array{Bound})
@mustimplement find(partition::Partition, point::Point)

include("./Samplers.jl")
include("./Planar.jl")
include("./WorldBuilders.jl")
include("./Arms.jl")
include("./ReedsShepp.jl")
include("./Splits.jl")
include("./Partitions/BinaryPartitions.jl")
include("./partitions/GridPartitions.jl")
include("./PLRs.jl")
include("./Analysis.jl")

end # module
