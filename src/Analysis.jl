#=
 * File: Analysis.jl
 * Project: src
 * File Created: Thursday, 31st October 2019 9:43:06 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 11th November 2019 9:21:45 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
module Analysis
using MetricTools, MetricRegressors

using ..MetricSpaces,
    ..MetricSpaces.Samplers

export avgerror

function avgerror(space::Space, regressor::Regressor, truedist::Function; nsamples=400::Integer)
    points::Array{Point} = grid(space, nsamples)
    totalerror::Float64 = 0
    error::Float64 = 0
    
    for point in points
        error = abs(truedist(point) - predict(regressor, point))
        totalerror += error
    end
    
    return totalerror / nsamples
end

end
