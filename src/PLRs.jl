#=
 * File: PLRs.jl
 * Project: src
 * File Created: Tuesday, 22nd October 2019 9:30:18 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Friday, 28th February 2020 12:14:26 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module PLRs

import PyPlot, PyCall

using MetricTools, MetricRegressors
using ..MetricSpaces, ..BinaryPartitions, ..Samplers, ..Splits

export PLR, getleaves, getnumparams, tse, mse

mutable struct PLR <: Regressor
    bp::BinaryPartition
    bounds::Array{Bound}
    maxdepth::Integer
    maxerror::Float64
    minsamples::Integer
    
    """
    Constructs an PLR to be trained traditionally using sample points
    with known distances
    """
    PLR(bounds::Array{Bound};
            maxdepth::Integer=5,
            maxerror::Float64=0.005,
            minsamples::Integer=length(bounds) + 2) =
        new(BinaryPartition(), bounds, maxdepth, maxerror, minsamples)
    PLR(space::Space;
            maxdepth::Integer=5,
            maxerror::Float64=0.005,
            minsamples::Integer=space.dim + 2) =
        new(BinaryPartition(), space.bounds, maxdepth, maxerror, minsamples)

    """
    Constructs an PLR dynamically, using a distance function and a sampler
    To generate samples on the fly
    """
    function PLR(space::Space, distfunc::Function;
            maxdepth=5::Integer,
            sampler=grid::Function,
            nsamples=20::Integer,
            maxerror=0.005::Float64,
            errorfunc=tse::Function,
            minsamples=space.dim + 2::Integer)
            
        function splitonerror!(node::PartitionNode)
            points::Array{Point} = sampler(space, nsamples; bounds=node.bounds)
            values::Array{Float64} = [distfunc(point) for point in points]
            regressor::Regressor = LinearRegressor()
            train!(regressor, points, values)
            node.bp.data = regressor

            # calculate the error, split if too large
            return errorfunc(points, values, regressor) > maxerror
        end
        
        # recursively construct the binary partition, sampling and regressing
        # for each created child
        settings::PartitionSettings = PartitionSettings(splitonerror!, maxdepth)
        bp::BinaryPartition = BinaryPartition(space.bounds, settings)
        
        return new(bp, space.bounds, maxdepth, maxerror, minsamples)
    end
end


"""
Trains an PLR using provided sample points and values
Before splitting, check that there are enough samples in the cell
"""
function MetricRegressors.train!(plr::PLR,
        points::Array{Point}, values::Array{Float64})
        
    minsamples = max(plr.minsamples, length(first(points)) + 1)
    samples::Dict{Array{Bound}, Tuple{Array{Point}, Array{Float64}}} =
        Dict{Array{Bound}, Tuple{Array{Point}, Array{Float64}}}()
    samples[plr.bounds] = (points, values)
    
    """
    Splits if we have enough samples and the error is not too high
    """
    function splitonerror!(node::PartitionNode)
        regressor::Regressor = LinearRegressor()
        points, values = samples[node.bounds]
        if length(points) < plr.minsamples
            println("Not enough samples! Required $minsamples.")
            return false
        end
        train!(regressor, points, values)
        node.bp.data = regressor

        # calculate the error, split if too large
        error::Float64 = mse(regressor, points, values)
        return error > plr.maxerror
    end

    """
    Sorts the samples at a partition into the two children
    """
    function sortsamples(node::PartitionNode)
        points, values = samples[node.bounds]
        leftpoints::Array{Point}, leftvalues::Array{Float64} = [], []
        rightpoints::Array{Point}, rightvalues::Array{Float64} = [], []

        for i in eachindex(points)
            if isleft(node.bp.split, points[i])
                push!(leftpoints, points[i])
                push!(leftvalues, values[i])
            else
                push!(rightpoints, points[i])
                push!(rightvalues, values[i])
            end
        end
        
        leftbounds, rightbounds = splitbounds(node.bounds, node.depth)

        # if not enough samples for regression, don't split this direction
        if length(leftpoints) >= minsamples
            samples[leftbounds] = (leftpoints, leftvalues)
        else
            node.bp.left = nothing
        end
        if length(rightpoints) >= minsamples
            samples[rightbounds] = (rightpoints, rightvalues)
        else
            node.bp.right = nothing
        end
    end
    
    # recursively construct the binary partition, sampling and regressing
    # for each created child
    settings::PartitionSettings = 
        PartitionSettings(splitonerror!, plr.maxdepth; oncreate=sortsamples)
    plr.bp = BinaryPartition(plr.bounds, settings)
end

"""
Predicts the value of a point using the PLR
"""
function MetricRegressors.predict(plr::PLR,
        point::Point)::Union{Nothing, Float64}
    bp::Union{Nothing, BinaryPartition} = find(plr.bp, point)
    if isnothing(bp) || isnothing(bp.data)
        return nothing
    end
    
    return predict(bp.data, point)
end

MetricRegressors.predict(plr::PLR,
        points::Array{Point})::Array{Union{Nothing, Float64}} =
    [predict(plr, point) for point in points]

"""
Plots an PLR in 2D
"""
function MetricTools.plot(plr::PLR;
        ax::PyCall.PyObject=PyCall.PyNULL(),
        color::String="red",
        alpha::Float64=0.5,
        clevels::Integer=10,
        flevels::Integer=50,
        truedistf::Union{Function, Nothing}=nothing,
        length::Integer=100,
        lines::Bool=true)
    if lines plot(plr.bp, plr.bounds; ax=ax, color=color, alpha=alpha) end
    if isnothing(truedistf)
        plot(distfunc(plr); bounds=plr.bounds,
            ax=ax, clevels=clevels, flevels=flevels, length=length)
    else
        plot(distfunc(plr), truedistf; bounds=plr.bounds,
            ax=ax, clevels=clevels, flevels=flevels, length=length)
    end
end

"""
Gets the leaves of an PLR
"""
BinaryPartitions.getleaves(plr::PLR) = getleaves(plr.bp, plr.bounds)

"""
Gets the number of parameters for the PLR
    nparams = (dimension + 1) * nleaves + 2 * nleaves - 1
"""
function MetricRegressors.getnumparams(plr::PLR)
    nleaves::Integer = length(getleaves(plr))
    return nleaves * (length(plr.bounds) + 1) + (2 * nleaves - 1)
end

"""
Prints an PLR
"""
function Base.println(plr::PLR)
    println(plr.bp, plr.bounds)
end
end
