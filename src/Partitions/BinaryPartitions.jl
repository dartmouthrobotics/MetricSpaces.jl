#=
 * File: BinaryPartitions.jl
 * Project: src
 * File Created: Tuesday, 22nd October 2019 7:04:32 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 26th February 2020 3:49:51 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module BinaryPartitions

import PyPlot, PyCall
using MetricTools
using ..MetricSpaces, ..Splits

export BinaryPartition, PartitionSettings, PartitionNode, splitbounds, getleaves, haschild


#region structs
"""
Settings for Binary Partition
"""
struct PartitionSettings
    shouldsplit::Function
    maxdepth::Integer
    oncreate::Function
    function PartitionSettings(shouldsplit::Function, maxdepth::Integer;
        oncreate=passoncreate::Function)
        return new(shouldsplit, maxdepth, oncreate)
    end
end

"""
struct for a binary partition of a space
    """
mutable struct BinaryPartition
    left::Union{Nothing, BinaryPartition}
    right::Union{Nothing, BinaryPartition}
    split::Split
    data::Any
    
    """
    Constructs based on bounds
    """
    BinaryPartition(; split=Split()::Split) =
        new(nothing, nothing, split, nothing)
end

"""
Axillary struct used for traversing binary partitions
"""
struct PartitionNode
    bp::Union{Nothing, BinaryPartition}
    bounds::Array{Bound}
    depth::Integer
end

"""
Constructs a binary partition of a space using the given settings
"""
function BinaryPartition(bounds::Array{Bound}, settings::PartitionSettings)::BinaryPartition
    root::BinaryPartition = BinaryPartition(; split=Split(1, midpoint(bounds[1])))
    function createchildren(node::PartitionNode)
        # set the split of the current node
        dim::Integer = (node.depth) % length(node.bounds) + 1
        center::Float64 = midpoint(node.bounds[dim])
        node.bp.split = Split(dim, center)
        
        # if not past max depth, create children which will be added to queue
        if node.depth >= settings.maxdepth return end
        node.bp.left = BinaryPartition()
        node.bp.right = BinaryPartition()
        
        settings.oncreate(node)
    end

    dfs(root, bounds, createchildren; condition=settings.shouldsplit)
    return root
end

#endregion structs

"""
Default functions
"""
alwayssplit(node::PartitionNode) = true
passoncreate(node::PartitionNode) = nothing

"""
Iterative depth first search through the partition
Keeping track of bounds and depth along the way
"""
function dfs(root::BinaryPartition, bounds::Array{Bound}, func::Function;
        condition=alwayssplit::Function)
    q::Array{PartitionNode} = [PartitionNode(root, bounds, 0)]
        
    while !isempty(q)
        node::PartitionNode = pop!(q)
        if !condition(node) continue end
        
        func(node)
        
        leftbounds, rightbounds = splitbounds(node.bounds, node.depth)
       
        if !isnothing(node.bp.left) 
            push!(q, PartitionNode(node.bp.left, leftbounds, node.depth + 1))
        end
        if !isnothing(node.bp.right) 
            push!(q, PartitionNode(node.bp.right, rightbounds, node.depth + 1))
        end
    end
end

"""
Splits the bounds, chosing dimension based on depth
"""
function splitbounds(
    bounds::Array{Bound},
    depth::Integer)::Tuple{Array{Bound}, Array{Bound}}

    dim = ((depth) % length(bounds)) + 1
    center = (bounds[dim].hi + bounds[dim].lo) / 2
    leftbounds, rightbounds = copy(bounds), copy(bounds)
    
    leftbounds[dim] = Bound(bounds[dim].lo, center)
    rightbounds[dim] = Bound(center, bounds[dim].hi)
    return leftbounds, rightbounds
end

@inline haschild(bp::BinaryPartition) = !(isnothing(bp.left) && isnothing(bp.right))

"""
Gets the 2 dimensional lines for the BP
"""
function MetricSpaces.getlines(bp::BinaryPartition, bounds::Array{Bound};
        maxdepth::Integer=100)::Array{Line}
    lines::Array{Line} = []

    function addlines(node::PartitionNode)
        if !haschild(node.bp) return end
        if node.bp.split.dim == 1
            push!(lines, ([node.bp.split.threshold, node.bounds[2].lo],
                [node.bp.split.threshold, node.bounds[2].hi]))
        elseif node.bp.split.dim == 2
            push!(lines, ([node.bounds[1].lo, node.bp.split.threshold],
                [node.bounds[1].hi, node.bp.split.threshold]))
        end
    end
    
    dfs(bp, bounds, addlines; condition=(n) -> n.depth < maxdepth)
    return lines
end

"""
Gets the 2 dimensional lines for the PLR
"""
function getleaves(bp::BinaryPartition, bounds::Array{Bound})::Array{BinaryPartition}
    leaves::Array{BinaryPartition} = []

    function addleaves(node::PartitionNode)
        if !haschild(node.bp) push!(leaves, node.bp) end
    end
    
    dfs(bp, bounds, addleaves)
    return leaves
end

function MetricSpaces.find(bp::BinaryPartition, point::Point)::Union{Nothing, BinaryPartition}
    while haschild(bp)
        if isleft(bp.split, point) && !isnothing(bp.left)
            bp = bp.left
        elseif !isnothing(bp.right)
            bp = bp.right 
        else
            return bp
        end
    end
    return bp
end

function Base.println(bp::BinaryPartition, bounds::Array{Bound})
    function printbp(node::PartitionNode)
        println("  "^node.depth, "$(node.depth): $(node.bounds), $(node.bp.split)")
    end
    
    dfs(bp, bounds, printbp)
end

"""
Plots a binary partition in 2D
"""
function MetricTools.plot(bp::BinaryPartition, bounds::Array{Bound};
        ax=PyCall.PyNULL()::PyCall.PyObject,
        color::String="black",
        alpha::Float64=0.5)
    
    linelists::Array{Array{Line}} = [getlines(bp, bounds; maxdepth=d) for d in 1:8]
    for lines in linelists 
        plot(lines; ax=ax, color=color, alpha=1/6, width=2)
    end
end
end
