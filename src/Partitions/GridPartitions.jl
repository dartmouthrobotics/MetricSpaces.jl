#=
 * File: GridPartitions.jl
 * Project: src
 * File Created: Friday, 10th January 2020 3:17:00 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 18th March 2020 11:52:08 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module GridPartitions

import PyPlot, PyCall
using MetricTools
using ..MetricSpaces, ..Splits

export GridPartition, getbounds

#region structs
"""
struct for a binary partition of a space
"""
mutable struct GridPartition
    sizes::Array{Float64}
    cells::Array{Any}
    bounds::Array{Bound}

    # uninitialized
    GridPartition() = new([], [], [])
    
    function GridPartition(
            sizes::Array{Float64},
            bounds::Array{Bound};
            initf::Function=(bounds) -> bounds,
            threaded::Bool=true)
            
        dim::Integer = length(bounds)
        lengths::Array{Integer} =
            [(b.hi - b.lo) ÷ sizes[d] for (d, b) in enumerate(bounds)]
        cells::Array{Any, dim} = Array{Any, dim}(nothing, lengths...)
        gp::GridPartition = new(sizes, cells, bounds)

        # init the cells
        iterators::Array{UnitRange} =
            [1:len for len in lengths]
        for indices in Iterators.product(iterators...)
            gp.cells[indices...] = initf(getbounds(gp, indices))
        end

        # if threaded
        #     allindices::Array{Tuple} = collect(Iterators.product(iterators...))
        #     Threads.@threads for indices in allindices
        #         gp.cells[indices...] = initf(getbounds(gp, indices))
        #     end
        # else
        # end

        return gp
    end
end
#endregion structs

"""
Gets the 2 dimensional lines for the GP
"""
function MetricSpaces.getlines(gp::GridPartition, bounds::Array{Bound})::Array{Line}
    lines::Array{Line} = []
    for thresh in bounds[1].lo:gp.sizes[1]:bounds[1].hi
        push!(lines, ([thresh, bounds[2].lo], [thresh, bounds[2].hi]))
    end

    for thresh in bounds[2].lo:gp.sizes[2]:bounds[2].hi
        push!(lines, ([bounds[1].lo, thresh], [bounds[1].hi, thresh]))
    end

    return lines
end

"""
Finds the cell that contains the given point
If getting points on boundary, call this function twice,
    once with bounddim = 0, and again with bounddim = boundary dimension
"""
function MetricSpaces.find(gp::GridPartition, point::Point;
        bounddim::Integer=0)::Union{Nothing, Any}
    indices::Array{Integer} = [
        (point[d] < gp.bounds[d].hi ? 1 : 0) +      # add 1 for all except upper bound
        (point[d] - b.lo) ÷ gp.sizes[d] for (d, b) in enumerate(gp.bounds)
    ]

    # handles points on boundary
    if bounddim > 0
        indices[bounddim] -= 1
        if indices[bounddim] < 1 return nothing end
    end
    
    return gp.cells[indices...]
end

"""
Gets the bounds of a cell
"""
function getbounds(gp::GridPartition, indices::Tuple)::Array{Bound}
    return [
        Bound(
            gp.sizes[d] * (indices[d] - 1) + b.lo,
            gp.sizes[d] * (indices[d]) + b.lo)
        for (d, b) in enumerate(gp.bounds)
    ]
end

"""
Plots a grid partition in 2D
"""
function MetricTools.plot(gp::GridPartition;
        ax=PyCall.PyNULL()::PyCall.PyObject,
        color::String="black",
        alpha::Float64=1/2)
    lines::Array{Line} = getlines(gp, gp.bounds)
    plot(lines; ax=ax, color=color, alpha=alpha, width=2)
end
end
