#=
 * File: ReedsShepp.jl
 * Project: src
 * File Created: Saturday, 21st December 2019 8:23:07 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 25th April 2020 11:07:18 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module ReedsShepp

import Distances
import PyCall, PyPlot
using SimpleCarModels
using MetricTools
using ..MetricSpaces, MetricTools.Geometry, MetricTools.GJKTools, MetricTools.EarClipping

export ReedsSheppSpace

"""
Struct for Reeds-Sheppe car
"""
mutable struct ReedsSheppSpace <: Space
    dim::Integer
    bounds::Array{Bound}
    obstacles::Array{Polygon}
    v::Float64
    r::Float64
    dt::Float64

    function ReedsSheppSpace(;
            bounds::Array{Bound}=[Bound(0, 1), Bound(0, 1), Bound(-pi, pi)],
            filename=""::String,
            v::Float64=1.,
            r::Float64=1.,
            dt::Float64=0.02)
        obstacles::Array{Polygon} = !isempty(filename) ? load_polygons(filename) : []
        return new(3, bounds, obstacles, v, r, dt)
    end
end

struct ReedsSheppMetric <: Distances.Metric
    thresh::Float64

    ReedsSheppMetric() = new(0.)
end


function (d::ReedsSheppMetric)(a, b)
    q0::SE2State{Float64} = SE2State{Float64}(a)
    qf::SE2State{Float64} = SE2State{Float64}(b)
    return reedsshepp_length(q0, qf)
end

MetricSpaces.dmetric(space::ReedsSheppSpace) = ReedsSheppMetric()

"""
Calculates the distance between two points
"""
function MetricTools.metric(space::ReedsSheppSpace, point1::Point, point2::Point)::Float64
    ReedsSheppMetric()(point1, point2)
end

"""
Determines if a point is free
"""
function MetricSpaces.isfree(space::ReedsSheppSpace, point::Point)::Bool
    for polygon in space.obstacles
        if contains(polygon, point) return false end
    end

    return true
end

"""
Determines if two points can be connected
"""
function MetricSpaces.localplanner(space::ReedsSheppSpace, point1::Point, point2::Point)::Bool
    points::Array{Point} = interpolate(space, point1, point2)
    for point in points
        if !isfree(space, point)
            return false
        end
    end
    
    return true
end

"""
Calculates optimal path between two points
"""
function interpolate(space::ReedsSheppSpace, point1::Point, point2::Point;
        timescale::Float64=1.)::Array{Point}
    q0::SE2State{Float64} = SE2State{Float64}(point1)
    qf::SE2State{Float64} = SE2State{Float64}(point2)
    return reedsshepp_waypoints(q0, qf, space.dt * timescale;
        v=space.v, r=space.r)
end



"""
Perform GJK to get a lower bound on the distance to nearest obstacle
"""
function MetricSpaces.cert(space::ReedsSheppSpace, point::Point)::Float64
    pt::Point = point[1:2]
    diss = gjkdist(space.obstacles, pt)
    obs_pts::Array{Point} = gjkdist_with_pt(space.obstacles, pt)
    if isempty(obs_pts) return -0.1 end
    mindist::Float64 = typemax(Float64)
    # println("----------------------")
    # for obs_pt in obs_pts
    #     for angle in -pi:pi/4:pi  ###TODO: what to do!!!
    #         temp::Point = [obs_pt..., angle]
    #         #############
    #         q0::SE2State{Float64} = SE2State{Float64}(point)
    #         qf::SE2State{Float64} = SE2State{Float64}(temp)
    #         # println("here!!!!")
    #         reedsshepp_cost_v, reedsshepp_control_v = reedsshepp(q0, qf, v=space.v, r=space.r)
    #         # println("the control is: ", reedsshepp_control_v)
    #         # println("the control v is: ", reedsshepp_control_v[1])
    #         println("the velocity is: ", reedsshepp_control_v[1].u[1], " ", reedsshepp_control_v[1].u[2])
    #         ##########
    #         dis = metric(space, point, temp)

    #         if dis < mindist mindist = dis end
    #     end
    # end
    # println("the distance of gjk is: ", diss)
    # println("the distance of rs is: ", mindist)
    # point += [0.2, 0.4]
##### V_max is 1?
    return diss

end

"""
Perform GJK to get the penetration depth
"""
function MetricSpaces.obscert(space::ReedsSheppSpace, point::Point; delta::Float64=0.0)::Float64
    pt::Point = point[1:2]
    mindis = penetratedepth(space.obstacles, pt)
    # obs_pts::Array{Point} = penetratedepth_with_pt(space.obstacles, pt)
    # if isempty(obs_pts) println("error!!!") end
    # maxdist::Float64 = typemin(Float64)
    # for obs_pt in obs_pts
    #     for angle in -pi:pi/4:pi  ###TODO: what to do!!!
    #         temp::Point = [obs_pt..., angle]
    #         dis = metric(space, point, temp)
    #         if dis > maxdist mindist = dis end
    #     end
    # end
    return mindis
end


"""
Plots the space
"""
function MetricTools.plot(space::ReedsSheppSpace; ax=PyCall.PyNULL()::PyCall.PyObject)
    if ax == PyCall.PyNULL() ax = PyPlot.gca() end
    plot(space.obstacles; ax=ax)
end

"""
Plots a path
"""
function MetricTools.plot(
        space::ReedsSheppSpace,
        path::Array{Point};
        ax=PyCall.PyNULL()::PyCall.PyObject,
        color::String="black")
    plot(space; ax=ax)
    
    # interpolate all points on the path
    points::Array{Point} = []
    lastpoint::Point = []
    for i in firstindex(path):lastindex(path) - 1
        append!(points, interpolate(space, path[i], path[i + 1]; timescale=2.))
        lastpoint = pop!(points)
    end
    
    push!(points, lastpoint)
    # println("points: $points")

    # plot all points as arrows
    plot(points; ax=ax, alpha=0., color=color, hasangles=true)
end
end
