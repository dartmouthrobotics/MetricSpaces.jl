#=
 * File: Splits.jl
 * Project: src
 * File Created: Thursday, 7th November 2019 1:45:31 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Thursday, 7th November 2019 1:47:25 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module Splits
using MetricTools

export Split, isleft, isright, midpoint

struct Split
    dim::Integer
    threshold::Union{Nothing, Float64}
    Split() = new(1, nothing)
    Split(dim::Integer) = new(dim, nothing)
    Split(dim::Integer, threshold::Float64) = new(dim, threshold)
end

isleft(split::Split, point::Point) = point[split.dim] <= split.threshold
isright(split::Split, point::Point) = point[split.dim] > split.threshold

midpoint(bound::Bound) = (bound.hi + bound.lo) / 2

end
