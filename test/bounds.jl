#=
 * File: bounds.jl
 * Project: test
 * File Created: Tuesday, 22nd October 2019 9:52:44 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 22nd October 2019 9:57:38 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using Test
if false
    include("../src/MetricSpaces.jl")
    using
        .MetricSpaces,
        .MetricSpaces.Planar
end
using
    MetricSpaces,
    MetricSpaces.Planar

function test_space_containment()
    space::Space = PlanarSpace()
    @test contains(space, [0., 0.])
    @test !contains(space, [1., 1.])
    @test contains(space, [0.5, 0.5])
    @test !contains(space, [-1., -1.])
    @test !contains(space, [1.1, 1.1])
    return true
end

@testset "Bounds tests" begin
    @test test_space_containment()
end
