#=
 * File: partition.jl
 * Project: test
 * File Created: Tuesday, 22nd October 2019 9:13:52 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Friday, 10th January 2020 3:39:52 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
using MetricTools
if false
    include("../src/MetricSpaces.jl")
    using
        .MetricSpaces,
        .MetricSpaces.Planar,
        .MetricSpaces.BinaryPartitions
end

using
    MetricSpaces,
    MetricSpaces.Planar,
    MetricSpaces.BinaryPartitions

function basic_bp()
    space::Space = PlanarSpace()
    shouldsplit::Function = (node) -> true
    maxdepth::Integer = 5
    settings::PartitionSettings = PartitionSettings(
        shouldsplit,
        maxdepth)
    bp::BinaryPartition = BinaryPartition(space.bounds, settings)
    return bp, space
end

function test_partition()
    println("Testing partition")
    bp, space = basic_bp()
    lines = getlines(bp, space.bounds)
    println(lines)
end

function test_plot()
    println("Testing partition plot")
    bp, space = basic_bp()
    println(bp, space.bounds)
    plot(bp, space.bounds)
end

function test_find()
    println("Testing find")
    bp, space = basic_bp()
    
    println(bp, space.bounds)
    println(find(bp, [0.2, 0.6]))
end

function test_print()
    println("Testing print")
    bp, space = basic_bp()
    println(bp, space.bounds)
end

# test_partition()
# test_plot()
test_find()
# test_print()
