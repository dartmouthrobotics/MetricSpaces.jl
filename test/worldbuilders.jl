#=
 * File: worldbuilders.jl
 * Project: test
 * File Created: Tuesday, 3rd December 2019 4:02:10 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 3rd December 2019 8:42:10 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#


import PyCall, PyPlot
using MetricSpaces.WorldBuilders
function test_worldbuilder()
    wb::WorldBuilder = WorldBuilder()
end

test_worldbuilder()
