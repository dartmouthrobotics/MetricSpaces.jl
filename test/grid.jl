#=
 * File: grid.jl
 * Project: test
 * File Created: Tuesday, 22nd October 2019 9:13:52 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Friday, 10th January 2020 5:27:51 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
using MetricTools
if false
    include("../src/MetricSpaces.jl")
    using
        .MetricSpaces,
        .MetricSpaces.Planar,
        .MetricSpaces.GridPartitions
end

using
    MetricSpaces,
    MetricSpaces.Planar,
    MetricSpaces.GridPartitions

function basic_gp()
    space::Space = PlanarSpace()
    sizes::Array{Float64} = [
        0.1, 0.1
    ]
    gp::GridPartition = GridPartition(sizes, space.bounds)
    return gp, space, sizes
end

function test_partition()
    println("Testing partition")
    gp, space, sizes = basic_gp()
    lines = getlines(gp, space.bounds)
    println(lines)
end

function test_plot()
    println("Testing partition plot")
    gp, space, sizes = basic_gp()
    plot(gp)
end

function test_find()
    println("Testing find")
    gp, space, sizes = basic_gp()
    display(gp.cells)
    cell = find(gp, [0.1, 0.65])
    display(cell)
    bounds = getbounds(gp, (1, 4))
    display(bounds)
end


# test_partition()
test_plot()
test_find()
# test_print()
