#=
 * File: rplrs.jl
 * Project: test
 * File Created: Thursday, 24th October 2019 9:55:15 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 26th February 2020 3:09:37 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using Test
using MetricTools, MetricRegressors

if false
    include("../src/MetricSpaces.jl")
    using
        .MetricSpaces,
        .MetricSpaces.Planar,
        .MetricSpaces.Samplers,
        .MetricSpaces.BinaryPartitions,
        .MetricSpaces.PLRs
else
    using
        MetricSpaces,
        MetricSpaces.Planar,
        MetricSpaces.Samplers,
        MetricSpaces.BinaryPartitions,
        MetricSpaces.PLRs
end

function eucldist2origin(point::Point)::Float64
    x, y = point
    return sqrt(x^2 + y^2)
end

function test_rplrs()
    space::Space = PlanarSpace()
    plr::PLR = PLR(
        space,
        eucldist2origin;
        maxdepth=6,
        maxerror=0.0005,
        nsamples=9)
    println(plr)
    plot(plr)
    println(predict(plr, [0.5, 0.5]))
end

function test_rplrs_train()
    space::Space = PlanarSpace()
    
    points::Array{Point} = grid(space, 16^2)
    values::Array{Float64} = eucldist2origin.(points)
    plr::PLR = PLR(space)
    train!(plr, points, values; maxerror=0.0005)
    println(plr)
    plot(plr)
end

function test_rplr_utils()
    space::Space = PlanarSpace()
    
    points::Array{Point} = grid(space, 15^2)
    values::Array{Float64} = eucldist2origin.(points)
    plr::PLR = PLR(space; maxerror=0.00001, maxdepth=10)
    train!(plr, points, values)
    println(plr.bp, space.bounds)
    
    leaves::Array{BinaryPartition} = getleaves(plr)
    numparams::Integer = getnumparams(plr)
    println("leaves: ", leaves)
    println("num params: ", numparams)
    plot(plr)
end

# test_rplrs()
# test_rplrs_train()
test_rplr_utils()
