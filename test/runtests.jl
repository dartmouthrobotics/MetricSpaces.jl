#=
 * File: runtests.jl
 * Project: test
 * File Created: Tuesday, 22nd October 2019 10:16:46 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 22nd October 2019 9:24:54 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test
#endregion imports

@testset "MetricSpaces Tests" begin
    println("Testing Spaces...")
    include("$(@__DIR__)/planar.jl")
    include("$(@__DIR__)/arms.jl")
    include("$(@__DIR__)/partition.jl")
end
