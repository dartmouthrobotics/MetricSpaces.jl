#=
 * File: arms.jl
 * Project: test
 * File Created: Tuesday, 22nd October 2019 10:16:56 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 3rd February 2020 9:20:13 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test
import PyPlot
using MetricTools

if false    # fix linter
    include("../src/MetricSpaces.jl")
    using 
        .MetricSpaces.Arms
end

using
    MetricSpaces.Arms
#endregion imports

function testarms()::Bool
    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms2.json", 
        "data/input/spaces/planar/world2.json"
    )
    plot(space)
    return true
end

function testarmanimation()::Bool
    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms2.json", 
        "data/input/spaces/planar/world2.json"
    )
    anglelists::Array{Point} = [
        [0, 0],
        [1, 1],
        [1.5, 2],
    ]
    
    plot(space, anglelists; video=false)
    return true
end

@testset "Arms Tests" begin
    PyPlot.close("all")
    # testarms()
    testarmanimation()
end
 