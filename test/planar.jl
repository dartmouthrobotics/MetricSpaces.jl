#=
 * File: planar.jl
 * Project: test
 * File Created: Tuesday, 22nd October 2019 10:16:56 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 18th March 2020 2:58:01 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test
using Flux
import PyPlot

using MetricTools, MetricRegressors

using
    MetricSpaces.Planar,
    MetricSpaces.Samplers

#endregion imports

function testload()
    space::PlanarSpace = PlanarSpace(filename="data/input/spaces/planar/world1.json")
    plot(space)
    return true
end

"""
Tests running regressions
"""
function regress()
    space::PlanarSpace = PlanarSpace()
    points::Array{Point} = grid(space, 16^2)
    origin::Point = [0, 0]
    
    regressors::Array{Regressor} = [
        LinearRegressor(),
        GaussianRegressor()
    ]

    for regressor in regressors
        train!(regressor, points, [space.metric(origin, x) for x in points])
    end
    
    truedist(p) = space.metric(origin, p)
    
    for regressor in regressors
        PyPlot.subplots()
        plot(truedist, distfunc(regressor))
        plot(points)
    end
    return true
end

function test_cert()
    space::PlanarSpace = PlanarSpace(filename="data/input/spaces/planar/world-concave.json")
    plot(space)
    point::Point = [0.3, 0.7]
    radius = cert(space, point)
    plot(point, radius)
end

test_cert()

# @testset "Planar Tests" begin
#     PyPlot.close("all")
#     @test testload()
#     @test regress()
# end
