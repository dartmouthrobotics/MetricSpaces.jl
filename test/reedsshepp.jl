#=
 * File: reedssheppe.jl
 * Project: test
 * File Created: Saturday, 21st December 2019 12:15:56 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 21st April 2020 8:50:37 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using MetricTools
using SimpleCarModels

if false
    include("../src/MetricSpaces.jl")
    using
        .MetricSpaces,
        .MetricSpaces.ReedsShepp
else
    using
        MetricSpaces,
        MetricSpaces.ReedsShepp
end

function test_reedsshepp()
    start::Point = [0, 0, pi/4]
    goal::Point = [0.9, 0.9, -pi/4]
    
    space::Space = ReedsSheppSpace()
    path::Array{Point} = [start, goal]
    println(metric(space, path))
    plot(space, path)
end

test_reedsshepp()
