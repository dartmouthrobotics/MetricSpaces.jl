using MetricTools
abstract type TestType end

@mustimplement f(t::TestType)

struct TestSubtype <: TestType
    num::Integer
end

# f(t::TestSubtype) = println(t.num)

f(TestSubtype(1))
